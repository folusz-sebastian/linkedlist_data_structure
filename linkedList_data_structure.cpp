#include <iostream>



class Element {
public:
    int data;
    Element* next;

    Element (int d){
        data=d;
        next=NULL;
    }
};

class List{
private:
    Element *head;
public:
    void add (int d);
    void add (int d, int nr);
    void remove (int nr);
    void show ();
    int find (int d);
    List (){
        head = NULL;
    }
};

void List::show() {
    Element* temp = head;
    while (temp) {
        std::cout<<temp->data;
        temp= temp->next;
    }
}

void List::remove(int nr) {
    if (nr == 1) {
        Element* temp = head;
        head=temp->next;
        delete(temp);
    }else{
            Element* prev = head;

            for (int i = 2; i < nr; ++i) {
                prev=prev->next;
            }

            Element* temp = prev->next;

            //if (prev->next->next == NULL) {
            //    prev->next=NULL;
            //}else{
                prev->next=prev->next->next;
            //}

        delete(temp);
         }
}

void List::add(int d, int nr) {
    Element* newElement = new Element(d);

    if (head == NULL) {
        head = newElement;

    } else if (nr==0){
        Element* temp = head;
        head = newElement;
        newElement->next = temp;
    }else if (nr==-1){
        Element* temp = head;
        while (temp->next) temp=temp->next;
        temp->next=newElement;
        newElement->next=NULL;
    } else{
        Element* prev = head;
        for (int i = 2; i < nr; ++i) {
            prev=prev->next;
        }
        Element* temp = prev->next;
        prev->next=newElement;
        newElement->next=temp;
    }

}

void List::add(int d) {
    Element* newElement = new Element(d);

    if (head == NULL) {
        head = newElement;

    } else {
        //Element* temp = head;
        //head = newElement;
        //newElement->next = temp;
        newElement->next = head;
        head = newElement;
    }
}

int List::find(int d) {
    int i = 1;
    Element *temp = head;
    while (temp){
        if (temp->data == d ) {
            std::cout << i;
            return  1;
        }
        else {
            temp = temp->next;
        }
        ++i;
    }
    std::cout<<"0";
    return 0;
    }

int main() {
    std::cout << "Hello, World!" << std::endl;
    List* list = new List;//   list->add(4);
    list->add(7);
    list->add(9);
    list->add(2);
    list->add(3);
    list->add(8, 5);
//    list->remove(5);
//    list->find(7);
    list->show();
    return 0;
}